package com.mjurinic.writernfc;

import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;
import android.util.Log;

import java.io.IOException;
import java.nio.charset.Charset;

public class MifareUltralightTagTester {

    private static final String TAG = MifareUltralightTagTester.class.getSimpleName();

    static public void writeTag(Tag tag) {
        MifareUltralight ultralight = MifareUltralight.get(tag);

        try {
            ultralight.connect();

            if (ultralight.isConnected()) {
                ultralight.writePage(6, "Swag".getBytes(Charset.forName("UTF-8")));

                Log.e(TAG, "Uspjesno zapisana poruka");
            }
            else {
                Log.e(TAG, "Can't open IO stream");
            }
        }
        catch (IOException e) {
            Log.e(TAG, "IOException while closing MifareUltralight...", e);
        }
        finally {
            try {
                ultralight.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException while closing MifareUltralight...", e);
            }
        }
    }
}
