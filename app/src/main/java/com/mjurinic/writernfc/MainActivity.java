package com.mjurinic.writernfc;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcF;
import android.nfc.tech.TagTechnology;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.Toast;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;


public class MainActivity extends ActionBarActivity {

    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mTagFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        IntentFilter mIntentFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mTagFilters = new IntentFilter[] { mIntentFilter };
    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.enableForegroundDispatch(this, mPendingIntent, mTagFilters, null);
    }

    @Override
    public void onNewIntent(Intent intent) {
        new TagWriter(intent).execute();
    }

    public class TagWriter extends AsyncTask<String, String, String> {
        private Tag scannedTag;
        private NdefRecord record;
        private NdefMessage message;

        Intent intent;

        TagWriter(Intent _intent) {
            intent = _intent;
        }

        protected void onPreExecute() {
            scannedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            record = NdefRecord.createMime("en", "Pristoja poruka".getBytes(Charset.forName("UTF-8")));
            message = new NdefMessage(new NdefRecord[] { record });
        }

        protected String doInBackground(String... params) {
            if (writeTag(message, scannedTag)) {
                //Toast.makeText(getApplicationContext(), "Write successful", Toast.LENGTH_SHORT);
                Log.e("Uspjesno zapisana poruka", "OK");
            }

            return null;
        }
    }

    public boolean writeTag(NdefMessage message, Tag tag) {
        int messageSize = message.toByteArray().length;

        try {
            Ndef ndef = Ndef.get(tag);
            NfcA nfa = NfcA.get(tag);
            IsoDep iso = IsoDep.get(tag);
            MifareUltralight mfu = MifareUltralight.get(tag);
            MifareClassic mfc = MifareClassic.get(tag);

            if (ndef != null) {
                ndef.connect();

                Log.e("NDEF", "Usel u NDEF");

                if (!ndef.isWritable()) {
                    Toast.makeText(getApplicationContext(), "Tag not writeable", Toast.LENGTH_SHORT);

                    return false;
                }

                if (ndef.getMaxSize() < messageSize) {
                    Toast.makeText(getApplicationContext(), "Message too big", Toast.LENGTH_SHORT);

                    return false;
                }

                ndef.writeNdefMessage(message);

                return true;
            }
            else if (mfu != null) {
                Log.e("MFU", "Usel u MFU");

                try {
                    mfu.connect();
                    mfu.writePage(4, "MFUL".getBytes());
                    mfu.close();

                    return true;
                }
                catch (IOException e) {
                    Log.e("MFU", e.getStackTrace().toString());

                    return false;
                }
            }
            else if (mfc != null) {
                Log.e("MFC", "Usel u MFC");

                try {
                    mfc.connect();
                    mfc.transceive("MFC".getBytes());
                    mfc.close();

                    return true;
                }
                catch (IOException e) {
                    Log.e("MFC", e.getStackTrace().toString());

                    return false;
                }
            }
            else if (nfa != null) {
                Log.e("NFA", "Usel u NFA");

                try {
                    nfa.connect();
                    nfa.transceive("NFCA".getBytes());
                    nfa.close();

                    return true;
                }
                catch (IOException e) {
                    Log.e("NFCA", e.getStackTrace().toString());

                    return false;
                }
            }
            else if (iso != null) {
                Log.e("ISO", "Usel u ISO");

                try {
                    iso.connect();
                    iso.transceive("ISO".getBytes());
                    iso.close();

                    return true;
                }
                catch (IOException e) {
                    Log.e("ISO", e.getStackTrace().toString());

                    return false;
                }
            }
            else {
                NdefFormatable format = NdefFormatable.get(tag);

                Log.e("Formatable", "Usel u formatable");

                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);

                        return true;
                    }
                    catch (IOException e) {
                        for(StackTraceElement er: e.getStackTrace()){
                            //Log.d("WHATEVA",er.toString());
                            e.printStackTrace();
                        }
                    }
                }

                return false;
            }
        }
        catch (Exception e) {
            Log.e("writeTag - outer", e.toString());

            return false;
        }
    }
}
